![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

# APACHE HADOOP INSTALLATION:

### HADOOP DOWNLOAD:

**Step 1:** Click on [Download Hadoop](https://hadoop.apache.org/release/3.1.2.html)

Select Download tar.gz file (green box) and hadoop tar file will be downloaded.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_27.png)

**NOTE:** if there is no option like WinRAR to extract the hadoop tar file then download it from this link WinRAR.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_28.png)

**Step 2:** Extract the downloaded file using WinRAR.

When you click on extract file this extraction path and options page will open.

Edit destination path as C:\ drive or any other drive preferred.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_29.png)

**After the extraction is completed, you will get this diagnostic message, just close it.**

**Step 3:** Then you will see Hadoop folder created. Open this folder all Hadoop files are inside this folder.

### EDITION OF HADOOP etc FILE:

To edit hadoop file you will need Notepad++.

Click on this link or copy paste the link on new tab.

[notepad++ Download](https://notepad-plus.en.softonic.com/download)

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_30.png)

**Lets start with editing now.**

**Step 1:** Open Hadoop folder --> open “etc” folder --> Inside you will find hadoop folder, open it:

We are going to edit 5 files here.

1st file= core-site (XML DOCUMENT) right click on this file and select option edit with notepad++.

Page will open like this:

NOTE: don’t forget to save all the files after you paste your values in it. Save it after editing forsure.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_31.png)

Copy and paste this value given below inside the configuration in core-site.xml file.

    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>
`

After pasting it, it will look like this.

![core](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_32.png)

In the same way edit hdfs-site, mapred-site, and yarn-site (XML document).

**To assist you**

Value for each file is given below.

**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **“namenode”** and **“datanode”**. And in value you must paste **path location** of your datanode and namenode.

![data](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_33.png)

**Values are:**

**Hdfs-site.xml**
   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value><path of namednode></value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value><path of datanode></value>

    </property>



**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

![hadoop cmd](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_34.png)

Edit here **JAVA_HOME= “variable value"** you entered when you were creating the environmental variable for JAVA”. Open your setting and see your variable value. 

**Note: When you enter the Variable Value kindly remove the "bin".**

**REMINDER: do not forget to save your edited files.**

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_35.png)

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the variable value of Hadoop bin folder here as well.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_36.png)

**Step 3:** Then add the path location of hadoop’s sbin folder as well and click ok.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_37.png)

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab.download it.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_38.png)

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it copy that bin folder and **replace** the previous bin. Now delete unnecessary files of Hadoop configuration.

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt in **Administrative mode** and change the directory to C:/Hadoop/sbin and type “hadoop version” and enter.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_39.png)

**Step 2:** Type “hdfs namenode -format” and enter.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_40.png)

**Step 3:** Type start-all.cmd and enter.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_41.png)

### Verify Hadoop installation via browser

**Localhost:8088**

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_42.png)


**Localhost:9870**

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_43.png)

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_44.png)

**Hadoop has been successfully installed.**

                                         **This ends the exercise**





















