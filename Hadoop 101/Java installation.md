![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

# HADOOP FUNDAMENTALS:

### LAB 1: SET UP: APACHE HADOOP

**Setting up your lab environment.**

In this lab, we will be using APACHE HADOOP for doing most of the hands-on labs. Apache Hadoop software is an open-source software that enables the distributed processing and efficient storage purpose of large datasets across multiple clusters of computers.

### Objective:

After completing this lab,you will be able to know:

1.How to install Java.

2.How to install HADOOP.

### PREREQUISITES TO INSTALL HADOOP:

-**JAVA 8**

#### JAVA DOWNLOAD:

**"Hadoop 3.0X supports only java 8"**

**Step 1:** Let us Install Java.

Click on [Download Java 8](https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html) 

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_1.png)

**Step 2:** Scroll down and you will get various versions of java 8 available for(Linux, macOS and windows). Select your version and click on the jdk links.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_2.png)

**Step 3:** After clicking on jdk link you will get this option shown below, just accept it and select download option .

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_3.png)

**Step 4:** After clicking on download option you will be directed to oracle login page. Create your account and sign in.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_4.png)

**Step 5:** As you sign in, you will see java 8 start downloading.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_5.png)

#### JAVA INSTALLATION:

**Step 1:**  Click on downloaded jdk file and select **“Next”**

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_6.png)

**Step 2:** Click on **“Next”** and check it must be installed in C: drive only.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_7.png)

**Step 3:** Then Click **“Change”**.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_8.png)

**Step 4:** In C: drive, create new folder **“JAVA”** and click on **“Next”**

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_9.png)

**Step 5:**  YAYYY!! Java is intalling.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_10.png)

**Step 6:** Go to **“Program files"** in C: drive, you will find a folder named there as “java” open it and copy the jdk folder from there.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_11.png)

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_12.png)

**Step 7:** Paste this jdk file in the folder created at installation time “JAVA’’ in C: drive. Open this JAVA folder and paste jdk file here.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_13.png)

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_14.png)

**NOTE:** Delete that java folder from program files in C: drive. There should be only one java file.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_15.png)

#### Environment variable setting for java.

**Step 1:** Click on start and go to **settings**.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_16.png)

**Step 2:** In setting go to **“system”** and then search for **“edit for environment variables”**.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_17.png)

**Step 3:** Select **“environment variables”** and click ok.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_18.png)

**Step 4:** Click on **“New”**.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_19.png)

**Step 5:** Write variable name as: **“JAVA_HOME”** and variable value is the path location of jdk bin.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_20.png)

**How to get the path location of jdk bin.**

Go to JAVA folder in C: drive < open jdk1.8.0_251 file < open bin. Then copy the path.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_21.png)

**Step 6:** Click on path.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_22.png)

**Step 7:** Open path, click on **“New”** and then paste the path location of jdk bin here also. Then click ok. Now you are done with setting up environment variables.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_23.png)

#### Verify the installation of Java.

Open Command prompt and type **“javac”** and enter.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_25.png)

To know the version of java installed type **“java -version”**. It will appear like this.

![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_26.png)

**Java has been successfully installed.**

                                                 **This ends the excercise**























