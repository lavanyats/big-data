![map](https://gitlab.com/Pallavi_rai/mapreduce-and-yarn-lab-2/-/raw/master/images/skillup.png)

#  **MapReduce and YARN**

## **Lab-2 Exercise**

### **Objective**

We will run 3 programs using MapReduce .

### Three programs are:

**1.** Pi

**2.** Sudoku puzzle

**3.** DFSIO Benchmark

# Let's run the command now:

Open cmd in Administrative mode (Press Windows+R to open the “Run” box. Type “cmd” into the box and then press Ctrl+Shift+Enter to run the command as an administrator) and change working directory to C:\HADOOP\sbin and start cluster by using command. In Mac and Linux use `sudo start-all.sh`

```
start-all.cmd
```
 
![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_1.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_2.png)


Turn off your safemode by this command 

```
hadoop dfsadmin -safemode leave
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_3.png)

Now we will run some interesting examples using mapreduce.

### 1.To check the examples available type command:

```
hadoop jar <<path of the file with extension>>
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_3.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_5_1_.png)


### Example 1

- To run the pi example with 16 maps and 100000 samples, run the following command:

```
hadoop jar <<jar file path with extension>> pi 16 100000
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_4.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_6.png)

### Example 2:

**To solve sudoku puzzle.**

Do yo know?? Mapreduce can solve sudoku puzzle in just few seconds!!!

**Sudoku** is a logic puzzle made up of nine 3x3 grids.

Just type some commands and puzzle is solved.

**STEP 1:** Download the file [Sudoku.txt](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/lab%20files/sudoku.txt.txt) and save it as sudoku.txt in your local system.

**STEP 2:** Now create a directory in HDFS

```
hadoop fs -mkdir /input_dir
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_7.png)

**STEP 3:** Now load sudoku.txt file in HDFS using **-put** command

```
hadoop fs -put <<"path of the file with extension">> /input_dir
```

Check the file in hdfs using:

```
hadoop fs -ls /input_dir
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_8.png) 

**Step 4:** You can verify the input file via browser also. Go to below given link:

<a href="http://localhost:9870">http://localhost:9870</a>


![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/had_43.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_1.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_2.png)

**STEP 5:** Execute the mapreduce program for solving this sudoku puzzle using command:

```
hadoop jar <<"jar file path with extension">> sudoku /input_dir(file name) /output_dir
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_6.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_6_1_.png)

### Example 3:

**Run the TestDFSIO Benchmark:**

**Step1:** Run TestDFSIO in write mode and create data. Type command:

```
hadoop jar <<"jar file path with extension">> TestDFSIO -write  -nrFiles 10 -fileSize 1000
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_8.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_15.png)

**Step 2:** Run TestDFSIO in read mode. Type command:

```
hadoop jar <<"jar file path with extension">> TestDFSIO -read  -nrFiles 10 -fileSize 1000
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_7.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_17.png)

**Step 3:** Clean up the TestDFSIO data. Type command:

```
hadoop jar <<"jar file path with extension">> TestDFSIO -clean
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_9.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_pic_12.png)

Stop all Hadoop resources before exiting the command prompt using `stop-all.cmd`. In Mac and Linux use `sudo stop-all.sh`

```
stop-all.cmd
```

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_19.png)

![map](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/map_20.png)



                                                           **This Ends the Excercise**


















