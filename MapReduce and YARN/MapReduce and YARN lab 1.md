![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# **MapReduce and YARN**

## **Lab-1 Exercise**

### **Objective**

To run the wordcount program using MapReduce.

### **Pre-requisite**

**1.** Java 8

**2.** Hadoop 3.X

### **Prepare:**

**1.** Locate the MapReduce example.jar file in &quot;Hadoop&quot; Folder. Common location will be

**C:\HADOOP\share\hadoop\mapreduce\hadoop-mapreduce-examples-3.1.2**

**Note: The path will change according to the user saving the file.**

**2.** Kindly find the .csv file to use in the lab exercise [books.csv](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/lab%20files/books.csv). Kindly place the lab in C:\ Drive or any other drive and keep a note of the path.

## **Let&#39;s run the commands now:**

**Step 1** - Open cmd in Administrative mode and move to &quot;C:/HADOOP/sbin&quot; and start cluster by using command.

**Start-all.cmd.**

![cmd](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/1.png)

![sbin](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/2.png)

![start-all](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/3.png)

**Step 2** - Move again to C:\ and if needed can run the rest of the script in non-safe mode otherwise can move to next step after moving back to C:\.

**hadoop dfsadmin -safemode leave**

![C drive](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/4.png)

![Safemode](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/5.png)

**Step 3** - Create an empty input directory/folder in HDFS using below given command.

**hadoop fs -mkdir /input\_dir**

![mkdir](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/6.png)

**Step 4** - Copy the input .csv file named books.csv in the input directory(input\_dir) of HDFS.

**hadoop fs -put <<"path of the file with extension">> /input\_dir**

![file](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/7.png)

**Step 5** - Verify that the input file is now available in HDFS directory(input\_dir).

**hadoop fs -ls /input\_dir**

![input](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/8.png)

**Step 6** – You can verify the input file via browser also. Go to below given link

**localhost:9870**

![browser](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/had_43.png)

**Step 7** – Here you can check the number of datanodes and namenodes also. Now to check the input file Go to **Utilities** -> **Browse the files system** -> Select the **input\_dir(the input file directory name).**

Inside that directory you can find the input file.

![Utilities](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/had_43.png)

![FileSystem](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_1.png)

![file](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_2.png)

**Step 8** – Now returning back to command line. Verify the content of the copied file.

**hadoop dfs -cat /input\_dir/<<"file\_name">>;**

![Verify](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/9.png)

**Step 9** - Run the Mapreduce.jar file and provide input and output directories.

**hadoop jar <<"jar file path with extension">> wordcount /input\_dir /output\_dir**

![jar](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_5.png)

![Exec](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/11.png)

**Step 10** - Verify content of the generated output file.

**hadoop dfs -cat /output\_dir/\***

![cat](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/12.png)

![output](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/13.png)

**Step 11** – You can verify the output file in browser.

**Localhost:9870**  ->  **Utilities**  ->  **Browse the file system**  ->  **output\_dir**  ->  **part-r-0000.**

This is the final output file which can even be downloaded.

![Directory](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_3.png)

![output](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/pic_4.png)

**Step 12** - You can continue add any number of files and if you need to delete some files from the directory.

**hadoop fs -rm -r /input\_dir/<<"filename">>**

![remove](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/14.png)

**Step 13** – To delete the input and output directory.

**hadoop fs -rm -r /input\_dir**

**hadoop fs -rm -r /ouput\_dir**

![removedirectory](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/15.png)

**Step 14** – Stop all Hadoop resources before exit the command prompt.

**Stop-all.cmd**

![stop](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/16.png)

![exit](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/MapReduce%20and%20YARN/images/17.png)

**In the next lab we will run some more MapReduce Program.**

                                                              

                                                                        **This Ends the Exercise**

