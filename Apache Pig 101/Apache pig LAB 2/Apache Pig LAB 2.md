![image](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# Lab 2: Hands-On activities on Grunt Shell:

In the previous lab, we have already installed **APACHE PIG** , and we were into the **GRUNT SHELL**.

### OBJECTIVE:
- is to make you familiar with basic Pig Operators and running commands from the grunt shell.

**LET'S RUN SOME COMMANDS NOW:**

**Step 1:** Execute Pig in local mode by running below command: 

**pig -x local**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_1.png)

**Step 2:** Download the lab file: employees.txt from the below link:

[employees.txt](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/lab%20files/employees.txt)

**Step 3:** After the file is downloaded, save the file in d: drive or any other location and keep a check on the **path** of the file.

**Step 4:** Now, we will Load the file in a variable: input_file using the below command: 
(replace the path with your path)

**input_file = LOAD 'd:/employees.txt' USING PigStorage(',')**

**as (Emp_ID: Int, Emp_Name:chararray, Emp_Location:chararray);**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_2.png)

**Step 5:** Now we will check the results using the DUMP operator:

**DUMP input_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_3.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_4.png)

## Executing Pig script in Batch Mode: 

We can also execute Pig Script in Batch Mode.

**Step 1:** Download the lab file from the below link and read the written script.

**NOTE:** Replace the path of the employees.txt file in the employees.pig file as per your location and save the file.

Download employees.pig file from this link [employees.pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/lab%20files/employees.pig.txt)

**Apache Pig LIMIT Operator**

The Apache Pig LIMIT operator is used to limit the number of output tuples. 

**Apache Pig ORDER BY Operator**

The Apache Pig ORDER BY operator sorts a relation based on one or more fields. It maintains the order of tuples.

**Step 2:** Now, we will execute the script from the Grunt shell using the exec command as shown below: (replace the path with your file location)

**exec d:/employees.pig.txt**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_5.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%202/images/pig_lab_6.png)

You can see the Top 5 records being pulled out in Descending order as written in the script. Similarly, you can practice various Pig operators, for e.g. GROUP, SPLIT etc.

**This ends the exercise here.**























