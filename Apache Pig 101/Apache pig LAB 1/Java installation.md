![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

# Hands-on Lab Exercise 1-Apache Pig(A high-level Programming language)

**WHY we use Apache Pig ?**

- Did you know that 200 lines of MapReduce program can be written in just 10 lines of **pig** code.

- It analyzes bulk datasets.

- Similar to pigs, who eats anything, it can work upon any kind of data.

# Objective

After completing this lab,you will be able to know:

 -  How to install **Apache pig**

# Content
1. **JAVA Download**.
2. **HADOOP Download**.
3. **Apache Pig Download**.

# The Prerequisite for installing Apache pig are:

- Java 8

- Hadoop 3.0x

#### JAVA DOWNLOAD:

**"Hadoop 3.0X supports only java 8"**

**Step 1:** Let us Install Java.

Click on [Download Java 8](https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html) 

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_1.png" alt="had" width="600"/>


**Step 2:** Scroll down and and choose the JDK which is meant for your Operating System (Windows/Mac/Linux/Solaris). 

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_2.png" alt="had" width="600"/>


**Step 3:** It asks you accept the Oracle License. Check the box to accept and click on download.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_3.png" alt="had" width="600"/>


**Step 4:** After clicking on download option you will be directed to oracle login page. Create your account and sign in.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_4.png" alt="had" width="600"/>


**Step 5:** As you sign in, you will see Java 8 start downloading.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_5.png" alt="had" width="600"/>

#### JAVA INSTALLATION:

**Step 1:**  Click on downloaded jdk file and select **“Next”**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_6.png" alt="had" width="600"/>


**Step 2:** Click on **“Next”** and choose where to install JDK. 

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_7.png" alt="had" width="600"/>

**Step 3:** Then Click **“Change”**.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_8.png" alt="had" width="600"/>

**Step 4:** In C: drive, create new folder **“JAVA”** and click on **“Next”**

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_9.png" alt="had" width="600"/>

**Step 5:**  Java is intalling.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_10.png" alt="had" width="600"/>

**Step 6:** Go to **“Program files"** in C: drive, you will find a folder named there as “java” open it and copy the jdk folder from there.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_11.png" alt="had" width="600"/>

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_12.png" alt="had" width="600"/>


**Step 7:** Paste this jdk file in the folder created at installation time “JAVA’’ in C: drive. Open this JAVA folder and paste jdk file here.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_13.png" alt="had" width="600"/>

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_14.png" alt="had" width="600"/>


**NOTE:** Delete that java folder from program files in C: drive. There should be only one java file.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_15.png" alt="had" width="600"/>

#### Environment variable setting for java.

**Step 1:** Click on start and go to **settings**.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_16.png" alt="had" width="600"/>

**Step 2:** In setting go to **“system”** and then search for **“edit for environment variables”**.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_17.png" alt="had" width="600"/>

**Step 3:** Select **“environment variables”** and click ok.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_18.png" alt="had" width="600"/>

**Step 4:** Click on **“New”**.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_19.png" alt="had" width="600"/>


**Step 5:** Write variable name as: **“JAVA_HOME”** and variable value is the path location of jdk bin.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_20.png" alt="had" width="600"/>

**How to get the path location of jdk bin.**

Go to JAVA folder in C: drive < open jdk1.8.0_251 file < open bin. Then copy the path.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_21.png" alt="had" width="600"/>


**Step 6:** Click on path.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_22.png" alt="had" width="600"/>

**Step 7:** Open path, click on **“New”** and then paste the path location of jdk bin here also. Then click ok. Now you are done with setting up environment variables.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_23.png" alt="had" width="600"/>

#### Verify the installation of Java.

Open Command prompt and type **“javac”** and enter.


<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_25.png" alt="had" width="600"/>

To know the version of java installed type **“java -version”**. It will appear like this.

<img src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_26.png" alt="had" width="600"/>

**Java has been successfully installed.**

                                                 **This ends the excercise**























