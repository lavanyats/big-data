![had](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/skillup.png)

# APACHE HADOOP INSTALLATION:

### HADOOP DOWNLOAD:

**Step 1:** Follow this link to [Download Hadoop](https://hadoop.apache.org/release/3.1.2.html)

Select Download tar.gz file (green box) and hadoop tar file will be downloaded.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_27.png" alt="drawing" width="600"/>


**NOTE:** if there is no option like WinRAR to extract the hadoop tar file then download it from this link <a href="https://www.win-rar.com/postdownload.html?&L=0">WinRAR</a>


**Step 2:** Extract the downloaded file using WinRAR.

When you click on extract file this extraction path and options page will open.

Edit destination path as C:\ drive or any other drive preferred.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_29.png" alt="drawing" width="600"/>


**After the extraction is completed, you will get this diagnostic message. Just close it.**

**Step 3:** Then you will see Hadoop folder created. Open this folder. All Hadoop files are inside this folder.

### EDITION OF HADOOP etc FILES:

To edit hadoop file you will need a text editor like Notepad++.

Click on this link or copy paste the link on new tab.

[notepad++ Download](https://notepad-plus.en.softonic.com/download)


**Lets start with editing now.**

**Step 1:** Open Hadoop folder --> open “etc” folder --> open hadoop folder:

We are going to edit 5 files here.

** core-site **

Right click on this file and select option edit with notepad++.

Page will open like this:

NOTE: don’t forget to save all the files after you paste your values in it. Save it after editing.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_31.png" alt="drawing" width="600"/>


Copy and paste this value given below inside the configuration tags in core-site.xml file.

    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>
`

After pasting it, it will look like this.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_32.png" alt="core" width="600"/>


**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **“namenode”** and **“datanode”**. We will paste **full path** of your datanode and namenode in value as shown in the example below

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_33.png" alt="core" width="600"/>


**Hdfs-site.xml**
   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value>C:\data\namenode\</value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value>C:\data\datanode\</value>

    </property>



**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_34.png" alt="core" width="600"/>


Edit here **JAVA_HOME** to be set to the full path of the `jdk` folder which contains `bin`. Save and exit.

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:** Click on `Start` and go to **Settings** -->**System** and then search for **Edit Environment Variables for Your account**and click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_35.png" alt="core" width="600"/>

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the path of Hadoop's bin folder. This is done for the system to recognize the hadoop executables through the command-line.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_36.png" alt="core" width="600"/>

**Step 3:** Then add the path of Hadoop’s sbin folder as well and click ok.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_37.png" alt="core" width="600"/>

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab.download it.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_38.png" alt="core" width="600"/>

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it copy that bin folder and **replace** the previous bin. Now delete unnecessary files of Hadoop configuration.

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt in **Administrative mode** and change the directory to C:/Hadoop/sbin and type “hadoop version” and enter.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_39.png" alt="core" width="600"/>

**Step 2:** Type “hdfs namenode -format” and enter.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_40.png" alt="core" width="600"/>

**Step 3:** Type start-all.cmd and enter.

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_41.png" alt="core" width="600"/>

### Verify Hadoop installation via browser

On the browser enter the address **http://localhost:8088**

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_42.png" alt="core" width="600"/>


On the browser enter the address **http://localhost:9870**

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_43.png" alt="core" width="600"/>

<img align="center" src="https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Hadoop%20101/images/had_44.png" alt="core" width="600"/>

If you are able to see the page rendered as per the abive images, it means Hadoop has been successfully installed on your system.

                                         **This ends the exercise**     

