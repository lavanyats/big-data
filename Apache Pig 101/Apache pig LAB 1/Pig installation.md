![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# APACHE PIG Download

### Objective:

The objective of this lab is to get you started with Apache pig 101.

## Pig Download:

**Step 1:** To download the Apache Pig, click on [Download Pig](https://downloads.apache.org/pig/)
 
Click on pig-0.17.0 and download pig-0.17.0.tar.gz file.

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_1.png)

**Step 2:** Extract pig-0.17.0.tar.gz file using WinRAR and save the **pig folder** after extraction in **hadoop folder**.

**NOTE:SAVE THE EXTRACTED PIG FILE IN HADOOP FOLDER ONLY**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_extract_1.png)

**Step 3:** Pig is extracting.

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_extract_2.png)

**Step 4:** pig-0.17.0 is created in hadoop folder.**RENAME** pig-0.17.0 as pig.

![PIG](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/PIG_3.png)

### SETTING UP THE ENVIRONMENT VARIABLES FOR APACHE PIG:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: **“PIG_HOME”** and variable value is the path location of bin.

![PIG](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/PIG_4.png)

**Step 2:** Then click on **“Path”** in system variables, select **“New”** enter the variable value of Pig bin folder here as well.

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/PIG_5.png)

### EDITION OF PIG FILES:

**Step 1:** GO to **pig folder** inside the hadoop folder> open bin > open pig.cmd file and edit it with Notepad++

edit here this=**HADOOP_BIN_PATH=C:\hadoop\libexec**

![edit](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_edit_1.png)

**Step 2:** Go to C:\hadoop\libexec and open hadoop-config.cmd file and edit it with Notepad++

edit here this=**HADOOP_HOME=C:\hadoop\bin**

![e](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig_edit_2.png)

###### Veryfying the installation of APACHE PIG.(COMMAND PROMPT)

**Step 1:** Open cmd and type command **"pig -version"**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig.cmd.png)

**Step 2:** To run pig type command **"pig or pig -x local"**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%201/images/pig.cmd_2.png)

You can see **Grunt shell** started.

**APACHE PIG IS RUNNING**

**In the next lab we will run some commands on "Apache Pig".**

                                                         **This Ends the Exercise.**
