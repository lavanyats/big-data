![image](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# LAB 3 – Using Pig Functions:

### OBJECTIVE:

- Is to teach you the basic **Pig functions** that you can run from the Grunt Shell. 

Apache Pig provides various built-in functions namely:
- eval
- load
- store
- math
- string
- bag
- tuple

In this lab we will come across few **Eval functions** of Pig.

**Downloading the Lab Files:** 

[Lab3.txt](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/lab%20files/Lab3.txt)

[Lab3(2).txt](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/lab%20files/Lab3_2_.txt)

Execute Pig in local mode by running below command: 

**pig -x local**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_24.png)


####  1.Apache Pig AVG Function:

The Apache Pig AVG function is used to find the **average** of given numeric values in a single-column bag. 

Let’s start with loading the file **[Lab3.txt](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/Apache%20pig%20LAB%203/lab%20files/Lab3.txt)** that contains the data using the below command: 

**input_file = LOAD 'd:/Lab3.txt' USING PigStorage(',')** 

**as (Emp_ID: Int, Emp_Name:chararray, Emp_percentile:float);**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_1.png)

Now, we will verify the data using the below command:

**DUMP input_file**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_2.png)

Now, let’s **Group** the data on the basis of Name field using below commands: 

**new_file = GROUP input_file BY Emp_Name;**  

**DUMP new_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_3.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_4.png)

Now, we will calculate the **average** using below command:

**Result = FOREACH new_file GENERATE input_file.Emp_Name, AVG(input_file.Emp_percentile);**
 
**Dump Result;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_5.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_6.png)

#### 2.Apache Pig COUNT Function:

The Apache Pig **COUNT function** is used to **count** the number of elements in a bag.

Let’s start with loading the file **[Lab3(2).txt](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/Apache%20pig%20LAB%203/lab%20files/Lab3(2).txt)** that contains the data using the below command: 

**input_file = LOAD 'd:/Lab3(2).txt' USING PigStorage(',')**

**as (col1:Int, col2:int,col3:int);**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_7.png)

Now, we will verify the data using the below command:

**DUMP input_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_8.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_9.png)

Now, let’s **Group** the data on the basis of col1 using below commands: 

**new_file = GROUP input_file BY col1;**  

**DUMP new_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_10.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_11.png)

Now, let’s use the **count** function:

**Result = FOREACH new_file GENERATE COUNT(input_file);**

**Dump Result;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_12.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_13.png)

#### 3.Apache Pig SUM Function:

The Apache Pig **SUM function** is used to find the sum of the numeric values in a single-column bag.

Let’s start with loading the file **[Lab3.txt](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/Apache%20pig%20LAB%203/lab%20files/Lab3.txt)** that contains the data using the below command:

**input_file = LOAD 'd:/Lab3.txt' USING PigStorage(',')**

**as (Emp_ID: Int, Emp_Name:chararray, Emp_percentile:float);**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_14.png)

Now, we will verify the data using the below command:

**DUMP input_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_15.png)

Now, let’s **Group** the data on the basis of Name field using below commands: 

**new_file = GROUP input_file BY Emp_Name;**

**DUMP new_file;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_16.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_17.png)

Now, we will calculate the SUM using below command:

**Result = FOREACH new_file GENERATE group, SUM(input_file.Emp_percentile);**

**Dump Result;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_18.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_19.png)

#### 4.Similarly, we can use Apache Pig MIN Function and Apache Pig MAX Function:

The Apache Pig **MIN function** is used to find out the **minimum** of the numeric values or chararrays in a single-column bag.

The Apache Pig **MAX function** is used to find out the **maximum** of the numeric values or chararrays in a single-column bag.

Now, instead of the SUM use the Min Function. The command for that will be as below: 

**Result = FOREACH new_file GENERATE group, MIN(input_file. Emp_percentile);**
  
**Dump Result;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_20.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_21.png)

Now, for **Max function** we will use below command: 

**Result = FOREACH new_file GENERATE group, MAX(input_file. Emp_percentile);**

**Dump Result;**

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_22.png)

![pig](https://gitlab.com/Pallavi_rai/big-data/-/raw/master/Apache%20Pig%20101/Apache%20pig%20LAB%203/images/pig_23.png)

                                                 **This ends the exercise**


































































